# --------------------------------------------------------
# (c) Copyright 2014 by Jason DeLaat.
# Licensed under BSD 3-clause licence.
# --------------------------------------------------------

from pymonad.List import List


class LazyList(List):
    """
    Represents a non-deterministic calculation or a calculation with more than one possible result.
    Based on Python's built-in `list` type, `List` supports most basic list operations such as
    indexing, slicing, etc.
    """

    def __init__(self, *values):
        """ Takes any number of values (including none) and puts them in the List monad. """
        super(LazyList, self).__init__(values)

    def fmap(self, function):
        """ Applies `function` to every element in a List, returning a new List. """
        for element in self:
            yield function(element)

    def bind(self, function):
        """
        Applies `function` to the result of a previous List operation.
        `function` should accept a single non-List argument and return a new List.
        """
        for list_element in self:
            for sublist_element in function(list_element):
                yield sublist_element
