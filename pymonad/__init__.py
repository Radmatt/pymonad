# --------------------------------------------------------
# (c) Copyright 2014 by Jason DeLaat.
# Licensed under BSD 3-clause licence.
# --------------------------------------------------------

from pymonad.Container import *
from pymonad.Functor import *
from pymonad.Applicative import *
from pymonad.Monad import *
from pymonad.Reader import *
from pymonad.Maybe import *
from pymonad.Either import *
from pymonad.List import *
from pymonad.Monoid import *
from pymonad.Writer import *
from pymonad.State import *


def monadic(function):
    """Allow functions to use Monads to avoid exceptions and null (None) pointer errors,
    but just use regular python function calls, not fmap or amap.

    I don't know if this is in the spirit of monads, BUT...
    If the function is given a parameter that is an instance of Nothing or Left,
    return the first such parameter instead of calling the function.
    Also, if the function does not return a Container instance, wrap the return
    value in a Result object, unless it is None and so return Nothing instead.
    """
    def wrapper(*args, **kwargs):
        checked_args = []
        for arg in list(args):
            if isinstance(arg, Left) or isinstance(arg, type(Nothing)):
                return arg
            try:
                checked_args.append(arg.getValue())
            except AttributeError:
                checked_args.append(arg)
        checked_kwargs = {}
        for keyword, arg in kwargs.items():
            if isinstance(arg, Left) or isinstance(arg, type(Nothing)):
                return arg
            try:
                checked_kwargs[keyword] = arg.getValue()
            except AttributeError:
                checked_kwargs[keyword] = arg

        try:
            return _wrap_variable(function(*checked_args, **checked_kwargs))
        except Exception as e:
            return Exceptional(e)

    return wrapper


def _wrap_variable(variable):
    if isinstance(variable, list):
        return [_wrap_variable(elem) for elem in variable]
    elif isinstance(variable, dict):
        return {key: _wrap_variable(val) for key, val in variable.items()}
    elif not isinstance(variable, Container):
        if isinstance(variable, Exception):
            return Error(variable)
        elif isinstance(variable, bool):
            return Result(variable) if variable else Nothing
        elif variable is None:
            return Nothing
        else:
            return Result(variable)
    else:
        return variable

